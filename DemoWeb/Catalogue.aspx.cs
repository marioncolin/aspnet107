﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DemoWeb
{
    public partial class Catalogue : System.Web.UI.Page
    {
        protected Personne p;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                p = new Personne();
                p.Nom = "Bob";
                p.Age = 42;

                ViewState["proprio"] = p;
            }
            else
            {
                p = (Personne)ViewState["proprio"];
            }

            Label2.Text = p.Nom;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label1.Text = "HELLO " + Label1.Text.ToUpper() + " !!!";
        }
    }
}