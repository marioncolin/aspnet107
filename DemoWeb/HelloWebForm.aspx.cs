﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DemoWeb
{
    public partial class HelloWebForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtNom.Text = "plip";
            }           
        }

        protected void bntOk_Click(object sender, EventArgs e)
        {
            lblMessage.Text = "Bonjour " + txtNom.Text;
            txtNom.Text = "";
        }

        protected void btnSouligne_Click(object sender, EventArgs e)
        {
            lblMessage.Font.Underline = true;
        }

        protected void btnItalique_Click(object sender, EventArgs e)
        {
            lblMessage.Font.Italic = true;
        }
    }
}